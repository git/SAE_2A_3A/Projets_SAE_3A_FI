# IA : découverte et implémentation des réseaux de neurones profonds

Contact : [Chafik SAMIR](mailto:chafik.samir@uca.fr)

## Description

L'objectif principal de ce projet est de permettre aux étudiants de découvrir et tester les réseaux de neurones profonds dans le cadre d'une application IA. 

En particulier, il s'agit de programmer et tester différentes architectures pour réaliser une solution de vision artificielle basée sur les réseaux de neurones profonds dans le cadre d'une classification du comportement animal à partir des caractéristiques visuelles, des images 2D et des vidéos. 

Des tests seront faits sur des bases de données réelles et en collaboration avec le CHU Clermont. Nous comparons en outre les performances de ce nouveau modèle pour la classification d'images 2D.

