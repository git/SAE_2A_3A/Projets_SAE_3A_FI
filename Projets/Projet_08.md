# BibiApp

Contact : [Marc CHEVALDONNÉ](mailto:marc.chevaldonne@uca.fr)

## Description

Refaire l'appli bibi-binaire en MAUI.

Le système bibi-binaire est une représentation graphique et orale des chiffres hexadécimaux, inventé par le compositeur et chanteur de chansons à texte Boby Lapointe, aussi passionné par les mathématiques.  
Son système est ingénieux et original : il est allé jusqu'à inventé une prononciation pour chaque chiffre ainsi qu'une représentation graphique. Les sons et les symbôles ont été choisis de manière logique pour permettre de comprendre les nombres de manière rapide et succincte. Il est possible de réaliser n'importe quelle opération.  

Ce système est présenté par François Fabre, artiste et chanteur scientifique, notamment dans son spectacle "Si le Bibi de Boby m'était compté". A sa demande, nous avons réalisé il y a quelques années une application iOS/Android (avec Xamarin) permettant de présenter le bibi-binaire, en lui ajoutant quelques outils pour apprendre à compter et calculer en bibi : une calculette, une machine, une horloge, etc.  

L'application, développée par des étudiants de l'IUT, est vieillissante et mériterait un coup de jeune. C'est l'objectif de ce projet : recoder l'application en MAUI et la déposer sur les stores (iOS et Android).

Vidéo de l'application actuelle : https://vimeo.com/250176048


