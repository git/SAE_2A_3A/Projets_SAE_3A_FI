# Algoboto

Contact : [Marc CHEVALDONNÉ](mailto:marc.chevaldonne@uca.fr)

## Description

Poursuite de l'appli mobile de robotique pour les enfants.  

Depuis 2014, le Club Info développe des applications mobiles permettant de piloter des robots Lego EV3. L'objectif est de permettre à des petits et tout petits (de 4 à 11 ans) de découvrir l'algorithmique, la programmation, le codage et la robotique. L'application s'inspire du Logo et de scratch et est utilisée dans les écoles en Auvergne (Puy de Dôme, Allier, Haute-Loire) mais aussi lors d'évènements, à l'étranger (Belgique et Espagne).  

La première version, **Lego Logo** a vu le jour en 2015 et fonctionnait sur tablette Windows.  
La seconde version, **MazingBot** est sortie en 2018 et fonctionne sur smartphone Android.  
Nous avons démarré la troisième version **Algoboto** l'année dernière et nous la voulons cross-platform. Le travail réalisé l'année dernière est remarquable, mais mérite néanmoins quelques améliorations qui seront apportées dans le cadre de ce projet. L'application est développée pour iOS et Android avec MAUI.  

Vous trouverez ici la documentation réalisée l'année dernière par les étudiants ainsi que des vidéos des réalisations précédentes : https://codefirst.iut.uca.fr/documentation/AlgoboTeam/docusaurus/algoboto-documentation/



