# Class?

![](https://moex.inria.fr/mediation/class/imgs/class-game-design2.png)

Contact : [Pascal LAFOURCADE](mailto:pascal.lafourcade@uca.fr)

## Description

Imaginez que vous vivez dans une forêt et que vous n'ayez que des baies comme nourriture. Il y a différentes sortes de baies : des noires, des rouges, des bleues, des grandes, des petites, des venant en grappe, etc. Peut-être que vous aimez les baies rouges granuleuses mais que vous trouvez les grosses bleues trop amères. Peut-être même que les petites vertes vous ont rendu malade. Vous utilisez donc les caractéristiques des baies pour choisir celles que vous allez manger. Cela vous permet de les classer : vous vous intéressez d'abord aux baies rouges et, ensuite, regardez si elles sont lisses ou granuleuses, rondes ou allongées.

Mais différentes personnes avec différentes préférences, différentes expériences ou provenant de contrées lointaines peuvent avoir des classifications différentes. Comment savoir quelles baies leur cueillir pour leur faire plaisir ou les échanger contre celles que vous aimez ?

Alors comment faisons-nous pour nous comprendre ? Par la simple coopération, il est possible de se faire une idée de la connaissance des autres. C'est, en particulier, ainsi que l'on apprend une langue étrangère : on a sa propre langue et on cherche à faire correspondre les mots, les catégories.

Nous avons développé le jeu de cartes Class? pour faire comprendre cela.

Vous trouverez plus de précisions sur les règles du jeu [ici](https://moex.inria.fr/mediation/class/index.html#French).

## Travail demandé

Développer une application Web et Mobile pour jouer au jeu *Class?*.

L'application devra permettre de jouer aussi bien avec des humains qu'avec une IA.
