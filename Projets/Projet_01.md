# Développement d'un site d'édition de texte collaboratif avec une IA

Contact : [Maxime PUYS](mailto:maxime.puys@uca.fr)

## Description

L'objectif est de développer un site web d'édition en ligne de documents textes (pouvant aller jusqu'à inclure une syntaxe type Markdown suivant l'avancement) qui permette à un utilisateur de rédiger des documents (rapports, emails, ouvrages, etc.) à l'aide d'une IA générative qui pourra ajouter des phrases, modifier des passages existants. Cet outil vise à ne plus obliger le passage par un prompt fixe puis attendre que l'IA propose une réponse unique, mais à permettre une édition du document au fur et à mesure.

L'IA sera basée sur l'utilisation de modèles OLLAMA afin d'être privée et sécurisée.

Suivant l'avancement, on pourra considérer un mode collaboratif à plusieurs utilisateurs, un mode de test de code, etc.

