# Maudit Mot Dit

![](https://www.cocktailgames.com/wp-content/uploads/2022/01/Maudit_mot_dit_visuel_1_BD.jpg)

Contact : [Pascal LAFOURCADE](mailto:pascal.lafourcade@uca.fr)

## Description

Un jeu original et plein de malice… chacun va devoir faire deviner un mot… sans qu'il soit trouvé trop tôt !

Comment jouer ?

1. Un joueur ouvre discrètement la boite et découvre un mot à faire deviner avec un nombre précis d'indices.
2. Il donne des indices un par un pour que son mot soit trouvé avec le bon nombre d'indices, ni plus ni moins…
3. Si un adversaire devine le bon mot trop tôt, il lui vole les points !

Saurez-vous tourner autour du mot ?

Exemple à 3 joueurs : Irma essaye de faire deviner le mot secret Poisson en 4 indices. Après avoir dit “Astrologie” et “Lune”, elle dit ” Queue” en 3e indice. Félix propose Poisson, la manche s'arrête immédiatement et lui seul gagne 3 points !

Mauvais sorts et Malédiction inclus, pour le plaisir de se faire des ennemis !

## Travail demandé

Développer une application Web et Mobile pour jouer au jeu *Maudit Mot Dit*.

L'application devra permettre de jouer aussi bien avec des humains qu'avec une IA dont on pourra choisir la difficulté parmi 3 niveaux.
