# Infopoly

Contact : [Cédric BOUHOURS](mailto:cedric.bouhours@uca.fr)

## Description

Infopoly, le monopoly de l'informatique.

Application multiplate-forme évidemment, mais avec un client lourd tout de même !

L'objectif de ce projet est de développer un jeu inspiré du Monopoly dédié à l’informatique. Cette version du Monopoly devra être dédié aux informaticiens (il ne s'agira plus d'acheter des rues mais par exemple des concepts informatiques (classes, héritage, pointeur, langage...)). La règle devra être adaptée en conséquence. Une première version du jeu permettra de jouer seul contre l'ordinateur. Une évolution du jeu sera de pouvoir jouer en réseau ou via internet avec des adversaires.

Des étudiants ont déjà travaillé sur ce jeu il y a quelques années, et il est temps de le mettre à jour. Faudra-t-il repartir de 0 ? Voilà la première question posée ! Un gros travail de prise en main du code sera donc nécessaire.

![](img/infopoly.png)