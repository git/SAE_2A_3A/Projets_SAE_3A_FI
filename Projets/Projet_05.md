# Chat2test

![](img/Projet_05_img1.webp)![](img/Projet_05_img2.png)

## Effectuer des tests d’interface par le langage naturel

Contact : [Sébastien SALVA](mailto:sebastien.salva@uca.fr)

## Description

Des outils, tels que Selenium Driver ou Robot Framework, existent depuis longtemps pour effectuer des tests fonctionnels (conformité, robustesse, sécurité) à partir d'interfaces d'applications (GUI). Cela amène une grande simplicité et souplesse pour contrôler des aspects métiers dans certaines applications. Si, si, vous le verrez en cours cette année.

Mais à chaque fois que l'interface change, ou est mise à jour, il faut revoir les tests.
Et vint le jour où l'homme créa le LLM. Et si on pouvait simplement interroger le LLM pour faire un test adapté à notre place ? Et ce indépendamment de l'interface ?

C'est ce que je vous demande de faire et mettre en place dans ce projet. Comment faire ? Dans le désordre : choisir le bon LLM, lui donner de bons prompts, lui dire comment faire pour générer du code Selenium et lui fournir le code de l'interface, lui permettre d'exécuter des bouts de test, lui dire quand un test est bon.
Nous nous limiterons à des applications Web, donc il suffira de lui fournir une page HTML+JS et un statut HTTP.

#### Les verrous / fonctionnalités principales sont :

- Donner de la connaissance au LLM via une technique simple type *few shoots* ou via un RAG sur Selenium Driver;
- Lui faire générer du code simple pour plusieurs actions de type (cliquer sur bouton, entrer data sur champ, entrer login et mot de passe, etc.) avec les prompts appropriés;
- Lui faire lire une page HTML pour cibler des éléments DOM ou lui faire exécuter des scripts Selenium qui le feront très bien;
- Interfacer le LLM avec des exécutables ou faire des scripts qui lancent le LLM et ensuite lancent du code.

#### Si les premiers verrous sont simples et réalisés rapidement, ces fonctionnalités seront étudiées :

- Calculer la confiance des résultats (je serai votre guide dans cette partie);
- Lui faire des tests de sécurité automatiquement ou presque.


Le sujet est exploratoire sur certains aspects. Si le projet n'arrive pas à terme, ce n'est pas grave en soit (don't panic). Ce qui compte c'est votre motivation, vos façons de contourner un problème et votre analyse du contexte pour réduire la part de risque. Dans tous les cas, nous apprendrons.
