# Développement d'un logiciel pédagogique destiné à des étudiants en biologie

Contact : [Maxime PUYS](mailto:maxime.puys@uca.fr)

## Description

Ce sujet est proposé en collaboration avec l'UFR de biologie de l'UCA qui fait remonter un besoin urgent de développer un logiciel pédagogique permettant à des étudiants d'apprendre le fonctionnement de la génétique en simulant des croisements de drosophiles (mouches) présentant différentes caractéristiques génétiques.

L'UFR de biologie proposera entièrement les connaissances sur la partie génétique, laissant aux étudiants la charge de développer le moteur de croisement et la GUI du logiciel. Il devra être en mesure de tourner sous Windows et pourra, par exemple, être développé à l'aide de XAML et .NET. Ce logiciel devra être extensible en permettant aux équipes de biologie d'ajouter du contenu lié à la génétique (nouveaux gènes, etc.) via un fichier JSON.

> L'UFR de biologie et l'IUT demandent un financement pour un stage à l'université et, en cas d'acceptation de l'UCA, l'un des étudiants pourra, s'il le souhaite, poursuivre le développement du logiciel dans le cadre de son stage.
