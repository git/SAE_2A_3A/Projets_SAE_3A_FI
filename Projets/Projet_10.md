# ChatIUT

Contact : à vous de trouver un encadrant… on vous propose juste l'idée… mais ça plaira à Sébastien SALVA ;-)

## Description

Les journées portes-ouvertes (ou autres événements informatifs) voient passer de nombreuses personnes désireuses d'obtenir des informations à propos des cursus proposés à l'IUT. Parfois le personnel est occupé à répondre à des questions de certaines, pendant que d'autres attendent. Certaines sont quelques fois trop timides pour poser leurs questions aux enseignants. Bref, les interactions ne sont pas toujours aussi simples et fluides qu'on aimerait.

Le but de ce projet serait donc de proposer une appli vitrine de l'IUT qui permette à une personne de poser des questions à une IA. Celle-ci se reposerait sur un petit modèle de langage (*SLM*) amélioré par une méthode RAG (*Retrieval Augmented Generation*). On aurait ainsi une solution auto-hébergeable qui aurait l'avantage de se reposer sur des documents de l'IUT pour offrir des réponses précises et adaptées.

