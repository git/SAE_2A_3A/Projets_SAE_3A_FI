# SAÉ de 3A FI

Voici les règles qui encadrent la réalisation de vos SAÉ pendant votre troisième année :

- Les deux SAÉ sont fusionnées autour du même projet, soit 250h.
- Les créneaux de travail sont pendant la première période en entreprise des alternants (qui débute le 21/10/2024).
- Le travail doit être en présentiel (hors coupure de Toussaint), à raison de 7 heures par jours, selon les indications de l'emploi du temps (certains jours sont chômés).
- Les étudiants doivent choisir des projets par groupe de 3, pris dans la promotion des FI.
- Les groupes et les affectations doivent être décidés au plus tard le vendredi 18/10/2024. Un tableau contenant les groupes et la répartition devra être envoyé à [Laurent PROVOT](mailto:laurent.provot@uca.fr).
- La soutenance terminale aura lieu le jeudi 19 décembre 2024.

### Management de projet

L'équipe pédagogique gérant cette partie a pris soin de rédiger des documents précis qui vous permettront de connaître leurs attentes. *Lisez-les bien !*

- [Attentes et consignes](Documents/GestionProjet_Consignes3AFI.pdf)


## Liste des projets

Les projets barrés n'ont pas intéressé les FI mais peuvent potentiellement être choisis par les FA.

2. [Logiciel pédagogique pour des étudiants en biologie](Projets/Projet_02.md)
3. ~~[Class? un jeu de classification](Projets/Projet_03.md)~~
4. ~~[Maudit Mot Dit](Projets/Projet_04.md)~~
5. [Chat2test](Projets/Projet_05.md)
6. ~~[Infopoly](Projets/Projet_06.md)~~
7. [IA et réseaux de neurones profonds](Projets/Projet_07.md)
8. ~~[BibiApp](Projets/Projet_08.md)~~
9. ~~[Algoboto](Projets/Projet_09.md)~~


## Affectations

1. [Logiciel pédagogique pour des étudiants en biologie](Projets/Projet_02.md) : Maxime PUYS
   * Maël DAIM (PM2)
   * Jean MARCILLAC (WEB1)
   * Loris OBRY (WEB2)
   * Maxime VIMPERE (PM1)

2. [IA et réseaux de neurones profonds](Projets/Projet_07.md) : Chafik SAMIR
   * Mathéo THIERRY (WEB1)
   * Jade VAN BRABANDT (WEB2)
   * Fançois YOUSSE (PM3)

